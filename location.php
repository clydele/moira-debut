<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moira's 18th Bday</title>
        <link rel="stylesheet" href="mystyle.css">
        <style>
            body{background-color:white;margin:0;}
            .directionsButton{position:relative;width:100%;height:fit-content;top:-15%;}
                    .directionsButton a{position:relative;height:50%;background-color:#680068;color:white;font:18pt Eras ITC;border-radius:25px 25px 25px 25px;text-decoration:none;}
                    .directionsButton a:hover{color:#680068;background-color:#BF8F00;}
            .aboutOkada{text-align:left;font:}
        </style>
    </head>
    
    <body>
        <div class='container'>
            <div class='header'>
                <div class='navigation'>
                    <div class='nav-item'>
                        <a href="index.php">HOME</a>                 
                    </div>
                    <div class='nav-item'>
                        <a href='photos.php'>PHOTOS</a>
                    </div>
                    <div class='nav-item'style='height:90px;width:90px'>
                        <img src='pics/flower.png'>
                    </div>
                    <div class='nav-item'>
                        <a href='program.php'>PROGRAM</a>           
                    </div>       
                    <div class='nav-item'>
                        <a href='rsvp.php'>RSVP</a>
                    </div>
                </div>
            </div>  
            <div class='body'>
            
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='pagetitle'>
                    <div style='font:20pt Book Antiqua;color:#680068;letter-spacing:1.5em;text-align:center'>
                    <p><b>LOCATION</b></p>
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='col-12'>
                    <img src='pics/18mobile.PNG'>
                </div>
                <div class='col-12'>
                        &nbsp;
                    </div>
                    <div class='col-12'>
                        &nbsp;
                    </div>
                    <div class='col-12'>
                        &nbsp;
                    </div>
                    <div class='col-12'>
                        &nbsp;
                    </div>
                <div class='bodycontent'>

                    <div class='col-12'>
                        &nbsp;
                    </div>
                    <div id='textHeading'>
                        <p><b>About Cove Manila</b></p>
                    </div>
                    <div id='textContent'>
                        <p>Cove Manila is Southeast Asia’s newest entertainment space with its indoor beach club and nightclub, boasting some of the most advanced audio, video and ambient lighting technologies that promise an unparalleled world-class experience.</p>
                        <p>The Indoor Beach Club features an advanced climate control system that emulates a cool bayside atmosphere for a picture-perfect “summer experience” all year long. Guests can experience the venue’s luxury cabanas with five-star hotel amenities and a private hot tub—all while enjoying remarkable views of the Manila Bay and the vast 9000-sqm venue.</p>
                        <p>Meanwhile, the Nightclub is home to the country’s first six-ring kinetic chandelier capable of about 10,000 visual and kinetic effects that engulf the audience in a trance-like experience on the dance floor. This along with the Funktion-One Sound System by world-renowned sound engineer Tony Andrews takes the venue’s multi-sensory experience to new heights. All of this combined completes an eventful day-to-night party found nowhere else.
                        </p><p>Okada Manila, New Seaside Drive, Entertainment City, Parañaque City, 1701, Metro Manila, Philippines</p>
                    </div>
                    <div class=col-12>
                        &nbsp;
                    </div>
                    <div class='directionsButton'>
                        <a href='https://www.google.com/maps/dir//Okada+Manila,+New+Seaside+Dr,+Entertainment+City,+Para%C3%B1aque,+1701+Metro+Manila/@14.5157421,120.9462737,13z/data=!3m1!4b1!4m9!4m8!1m0!1m5!1m1!1s0x3397cc1be21135ef:0x8cb7cb6671d7f4e9!2m2!1d120.9812927!2d14.5156608!3e0'>Get Directions</a>
                    </div>
                </div>
                
                <div class=col-12>
                    &nbsp;
                </div>


                <div class='aboutOkada'>
                    Details from: <a href="https://covemanila.com/about/">https://covemanila.com/about/</a>
                </div>

                
            </div>
            <div class='footer'>
            </div>
        </div>
    </body>
</html>
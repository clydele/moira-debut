<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moira's 18th Bday</title>
        <link rel="stylesheet" href="mystyle.css">
        <style>
            img{object-fit:cover;}
            .col-1 {width: 8.33%;}
            .col-2 {width: 16.66%;}
            .col-3 {width: 25%;}
            .col-4 {width: 33.33%;}
            .col-5 {width: 41.66%;}
            .col-6 {width: 50%;}
            .col-7 {width: 58.33%;}
            .col-8 {width: 66.66%;}
            .col-9 {width: 75%;}
            .col-10 {width: 83.33%;}
            .col-11 {width: 91.66%;}
            .col-12 {width: 100%;}

            #indexWhole{display:block;}
            #indexHalf1{display:none;}
            #indexHalf2{display:none}
            
            body{background-color:white;margin:0;}
            .bodycontent-2{width:100vw;height:100vh;background-image:url(pics/2.PNG);background-size:contain;background-repeat:no-repeat;background-position:center;background-color:white;}
            .bodycontent-3{width:100vw;height:100vh;background-image:url(pics/3.png);background-size:contain;background-repeat:no-repeat;background-position:center;background-color:white;}
            .bodycontent-4{width:100vw;height:100vh;background-image:url(pics/4.png);background-size:contain;background-repeat:no-repeat;background-position:center;background-color:white;}
            
           
        </style>
    </head>
    
    <body>
        <div class='container'>
            <div class='header'>
                <div class='navigation'>
                    <div class='nav-item'>
                        <a href='index.php'>HOME</a>             
                    </div>
                    <div class='nav-item'>
                        <a href='program.php'>PROGRAM</a>
                    </div> 
                    <div class='nav-item'style='height:90px;width:90px'>
                        <img src='pics/flower.png'>
                    </div>
                    <div class='nav-item'>
                        <a href='location.php'>LOCATION</a>
                    </div>
                    <div class='nav-item'>
                        <a href='rsvp.php'>RSVP</a>
                    </div>
                </div>
            </div>  
            <div class='body'>

                <div class='col-12'>
                    &nbsp;
                </div>
                
                <div class='pagetitle'>
                    <div style='font:20pt Book Antiqua;color:#680068;letter-spacing:2em;text-align:center'>
                    <p><b>PHOTOS</b></p>
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/1.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/2.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/3.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/4.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/5.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/6.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/7.PNG'>
                </div>
                <div class='col-12'>
                    <img src='pics/PHOTOS/8.PNG'>
                </div>
               
                <div class='bodycontent2'>
                    <img src='pics/photos3.PNG'>
                </div>
                
            </div>
            <div class='footer'>
            </div>
        </div>
    </body>
</html>

<?php
    $con = new mysqli("localhost","oams","oams@0108","db_moira_debut")
?>
<!DOCTYPE html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moira's 18th Bday</title>
        <link rel="stylesheet" href="mystyle.css">
        <style>
            img{object-fit:cover;}
            .col-1 {width: 8.33%;}
            .col-2 {width: 16.66%;}
            .col-3 {width: 25%;}
            .col-4 {width: 33.33%;}
            .col-5 {width: 41.66%;}
            .col-6 {width: 50%;}
            .col-7 {width: 58.33%;}
            .col-8 {width: 66.66%;}
            .col-9 {width: 75%;}
            .col-10 {width: 83.33%;}
            .col-11 {width: 91.66%;}
            .col-12 {width: 100%;}

            #indexWhole{display:block;}
            #indexHalf1{display:none;}
            #indexHalf2{display:none}

            .firstPic{width:100%;position:relative;max-width:auto;height:auto;margin:auto auto;text-align:center;justify-content:space-around;display:flex;flex-wrap:wrap;}
            
            body{background-color:white;margin:0;}
            .bodycontent-2{width:100vw;height:100vh;background-image:url(pics/2.PNG);background-size:contain;background-repeat:no-repeat;background-position:center;background-color:white;}
            .bodycontent-3{width:100vw;height:100vh;background-image:url(pics/3.png);background-size:contain;background-repeat:no-repeat;background-position:center;background-color:white;}
            .bodycontent-4{width:100vw;height:100vh;background-image:url(pics/4.png);background-size:contain;background-repeat:no-repeat;background-position:center;background-color:white;}
            .seatForm{font:12pt Book Antiqua;color:#BF8F00;}
            .seatForm input[type='submit']{width:30%;font:12pt Book Antiqua}
            .seatForm input[type='text']{width:70%;font:12pt Book Antiqua}
            .Announcement{background-color:blue;}
            .btnseat{background-color:#680068;font:12pt Book Antiqua;color:white;}
            .btnseat:hover{background-color:white;color:#680068;}

            #seatReveal{color:#680068;margin:auto auto;}

            @media only screen and (max-width: 768px) {
                    /* For mobile phones: */
                    
                  
                    #indexWhole{display:none;}
                    #indexHalf1{display:block;width:100%;}
                    #indexHalf2{display:block;width:100%;}
                    
                    img{width:100%}
                    
                    .viewMoreBtn{position:relative;width:100%;height:fit-content;margin:auto auto;top:-15%;left:33%;}
                    .viewMoreBtn input{position:relative;height:0%;background-color:#680068;color:white;font:13pt Eras ITC;border-radius:20px 20px 20px 20px;}
            }
          
        </style>
        <script>
            function seatArrangement()
            {
                    var recordSent=document.getElementsByName("tb_Name")[0].value.toString();
                    var recordData=document.getElementsByName("recordshandler")[0].value.toString();
                    var recordspecific=recordData.split("|");
                    var a=0;
              
                    while(a<recordspecific.length)
                    {
                        if(recordspecific[a].localeCompare(recordSent)==0)
                        {
                            return showSeatNumber(recordspecific[a+1],recordSent);
                        }
                        else
                        {
                            a++;
                        }
                       
                    }
                  
            }
            function showSeatNumber(passedValue,passedName)
            {
                var original=document.getElementById("seatReveal").innerHTML;
                var stringOfName="<br><b>Name</b>:"+passedName;
                var stringOfSeat="<br><b>Seat Number: </b>"+passedValue;

                document.getElementById("seatReveal").innerHTML=stringOfName+stringOfSeat;
            }
        </script>
    </head>
    
    <body>
        <div class='container'>
            
          
            <div class='body'>
                <div class='firstPic'>
                     
                        <div class='col-6'id='indexHalf1'>
                            <img src='pics/front1.PNG'> 
                        </div>
                       
                                        
                </div>
                <div class='pagetitle'>
                    <div style='font:18pt Book Antiqua;color:#680068;letter-spacing:0.5em;text-align:center'>
                    <p><b>SEATPLAN</b></p>  
                    </div>
                </div>
                <div class='formBody'>
                    <div class='col-12 seatForm'>
                        <div class='row' >
                            <div class='col-6'style='text-align:center;margin:auto auto;'>

                                Please type your name to see your Assigned Seat<br>
                                <input type='text'name='tb_Name'list='AttendeeList'>
                                <datalist id='AttendeeList'>
    
                                    <?php 
                                    $records="";
                                    $assignedSeatsSQL="Select* from tbl_seatplan";
                                    $assignedSeatss=$con->query($assignedSeatsSQL);
                                    if($assignedSeatss->num_rows>0)
                                    {
                                        while($assignedSeat=$assignedSeatss->fetch_assoc())
                                        {
                                            echo"<option>".$assignedSeat["PersonAssigned"]."</option>";
                                            $records.=$assignedSeat["SeatID"]."|".$assignedSeat["PersonAssigned"]."|".$assignedSeat["SeatNumber"]."|";
                                        }
                                    
                                    }
                                    else
                                    {
                                        echo $con->error;
                                    }
                                    echo"<input type='hidden'name='recordshandler'value='".$records."'";


                                    ?>
                                </datalist>
                               
                            </div>

                            <div class='col-6'style='margin:auto auto;'>
                                <br>
                                <input type='button'name='btn_checkSeat'value=' Check Assigned Seats'class='btnseat'onclick='return seatArrangement();'>
                           
                            </div> 
                            <div id='seatReveal'class='col-6'>
                        
                                    
                            </div>
                        </div>
                    </div>
                </div>
                
                
            </div>
            <div class='footer'>
            </div>
        </div>
    </body>
</html>

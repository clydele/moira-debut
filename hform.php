<?php
                                                                                                                                                                                                                                     
$con=new mysqli("localhost","oams","oams@0108","db_moira_debut");
if(isset($_POST['btn_submitResponse']))
{
    $saveRSVP="INSERT INTO tbl_rsvp VALUES(null,'".addslashes($_POST['tb_rsvpName'])."','".addslashes($_POST['radio1'])."')";
    if($con->query($saveRSVP)===true)
    {
        echo"<script>alert('Response Saved');</script>";
    }
}
if(isset($_POST['btn_declineConfirmed']))
{
    $saveRSVP="INSERT INTO tbl_rsvp VALUES(null,'".addslashes($_POST['tb_rsvpName'])."','Regretfully Declines')";
    if($con->query($saveRSVP)===true)
    {
        echo"<script>alert('Response Saved');</script>";
    }
}


?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moira's 18th Bday</title>
        <link rel="stylesheet" href="mystyle.css">
        <style>
            body{background-color:#white;margin:0;}
            .rsvpForm{font:14pt Arial;width:100%;margin:auto auto;position:relative;}
            .rsvpForm input[type='text']{width:50%;margin:auto auto;position:relative;}
            .rsvpForm textarea{width:50%;height:10%;margin:auto auto;position:relative;}
            .purple{color:#680068;}
            .gold{color:#BF8F00;}
            .white{color:white;}
            #rsvpButton input{background-color:#680068;font:16pt Arial;border:5px 5px 5px 5px;color:white;text-align:center;}
            #rsvpButton input:hover{color:#680068;background-color:#BF8F00;}
            #rsvpButton2 input{background-color:#680068;font:12pt Arial;text-align:center;width:80%;color:#BF8F00;}
            #rsvpButton2 input:hover{color:#680068;background-color:#BF8F00;}
            #modal{z-index:1;position:fixed;background-color:white;display:none;font:15pt Book Antiqua;color:#680068;min-height:80px;vertical-align:middle !important;}
            .modalButton{background-color:white;color:#680068;border:0;font:15pt Book Antiqua;}
            .modalButton:hover{background-color:#680068;color:white;}
            .healthDecForm{height:600px;z-index:0;opacity:0.3;}
            .healthDecForm table{font:12pt Book Antiqua;color:#680068;}
            .healthDecForm table tr td{padding:5pt;}

            .disabledDiv{margin-top:-600px;height:600px;vertical-align:middle;display:flex;z-index:1;font:25pt Book Antiqua;color:#BF8F00;background-color:white;}
           
            .vaccinationStatusdiv{font:12pt Book Antiqua;color:#680068;width:90%;margin:auto auto;text-align:left;}
            .vaccinationStatusdiv table{font:12pt Book Antiqua;color:#680068;}
                .vaccinationStatusdiv table tr td{padding:3pt;}
            
            
        </style>
        <script>
            function hideModal()
            {
                document.getElementById('modal').style.display='none';
            }
            function ResponseSubmit()
            {
               // document.getElementById('modal').style.display="block";
                //var descision1=document.getElementsByName('radio1')[0].value;
               // alert(decision1);
                if(document.getElementById('acceptOption').checked)
                {
                    document.getElementById('modal').style.display='block';
                    document.getElementById('modal').innerHTML="<b>Great!See you there</b><br><br><input type='submit'name='btn_submitResponse'value='Ok'class='modalButton'>";
                                 
         
                }
                if(document.getElementById('declineOption').checked)
                {
                    document.getElementById('modal').style.display='block';
                    document.getElementById('modal').innerHTML="<b>Are you sure?</b><br><br><input type='submit'name='btn_declineConfirmed'value='Yes'class='modalButton'><input type='button'value='No'onclick='return hideModal();'class='modalButton'>";
                    
                }

            }
            
        </script>
    </head>
    5
    <body>
        <div class='container'>
            <div class='header'>
                <div class='navigation'>
                    <div class='nav-item'>
                        <a href="index.php">HOME</a>                 
                    </div>
                    <div class='nav-item'>
                        <a href='program.php'>PROGRAM</a>           
                    </div>  

                    <div class='nav-item'style='height:90px;width:90px;'>
                        <img src='pics/flower.png'>
                    </div>
                    <div class='nav-item'>
                        <a href='photos.php'>PHOTOS</a>
                    </div>
                    <div class='nav-item'>
                        <a href='location.php'>LOCATION</a>
                    </div>
                </div>
            </div>  
            <div class='body'>
                <div class='rsvpForm'>
                
                <div class='col-12'>
                 &nbsp;
                </div> 
                <div class='pagetitle'>
                    <div style='font:20pt Book Antiqua;color:#680068;text-align:center'>
                    <p><b>HEALTH DECLARATION FORM</b></p>
                </div>
                <div class='vaccinationStatusdiv'>
                <form method='POST'>
                  
                           <b> VACCINATION STATUS</b>
                     
           
                    <br>
                    <p><b>Note:</b>
                    <ul>
                    <li>As mandated by OKADA, only fully vaccinated adults are allowed to enter the venue.</li>
                    <li>Attendees are required undergo SarsCov Swab Test</li>
                    </ul>
                    </p>
                </form>
                </div>
                
              
                <div class='col-12 healthDecForm'>
                    <table>
                        <tr>
                            <td colspan='3'>
                            <b>Please respond to the following questions truthfully and to the best of your ability</b>
                            </td>
                        </tr>
                        <tr>
                            <td>Fever(100° F/37.8° or greater is measured by an oral thermometer)</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Cough?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Shortness of breath or difficulty breathing?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Sore throat?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Lost of taste or smell?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Chills?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Head or muscle aches?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            <td>Nausea, diarrhea, vommiting?</td>
                            <td><input type='radio'name='btn_vacStat'value='Fully Vaccined'>Yes</td>
                            <td><input type='radio'name='btn_vacStat'value='Not Fully Vaccined'>No</td>
                        </tr>
                        <tr>
                            
                            <td colspan='3'id='rsvpButton'><input type='submit'name='btn_submitDec'value='Submit Declaration Form' disabled/></td>
                        </tr> 
                        
                    </table>
                </div> 
                <div class='col-12 disabledDiv'>
                    <br><br><br><br><br>
                    Form not yet fillable. Will be available on October 30 2021.
                </div>
            

               
            </div>  
            <div class='footer'>
            </div>
        </div>
    </body>
</html>

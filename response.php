<?php
    $con=new mysqli("localhost","oams","oams@0108","db_moira_debut")
?>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moira's 18th Bday</title>
        <link rel="stylesheet" href="mystyle.css">
        <style>
            body{background-color:#DAB9F5;margin:0;}
            .programDiv{background-color:white;width:100vw;height:fit-content;}
            .programTitle{font:50pt Elegante;width:70%;color:#BF8F00;margin:auto auto;line-height:140%;}
            .programContent{font:17pt Book Antiqua;width:70%;margin:auto auto;line-height:140%;color:#680068;background-image:url('pics/programBackground.png');background-size:contain;background-position:center;background-repeat:no-repeat;}
            #desktop{display:block;}
            #mobile{display:none;}
            @media only screen and (max-width: 768px) {
                /* For mobile phones: */
                
                [class*="col-"] {
                    width: 100%;
                }
                #desktop{display:none;}
                 #mobile{display:block;}
                 .programTitle{font:35pt Elegante;}
                 .programContent{font:14pt Book Antiqua;}
            }
                </style>
    </head>
    
    <body>
        <div class='container'>
            <div class='header'>
                <div class='navigation'>
                    <div class='nav-item'>
                        <a href="index.php">HOME</a>
                       
                    </div>
                    <div class='nav-item'>
                        <a href='photos.php'>PHOTOS</a>
                      
                       
                    </div>
                    <div class='nav-item'>
                        <a href='location.php'>LOCATION</a>
                    </div>
                    <div class='nav-item'>
                        <a href='rsvp.php'>RSVP</a>
                    </div>
                </div>
            </div>  
            <div class='body'>
                <div class='bodycontent2'id='desktop'>
                    
                
                   
                        <img src='pics/7.PNG'>
                 
                </div>
            
                <div class='bodycontent2'id='mobile'>
                    
                
                    <div class='col-12'>
                        <img src='pics/programPic.PNG'>
                    </div>
                    
                </div>
                <div class='bodycontent2'>
                    <div class='programDiv'>
                        <div class='programTitle'>
                            Accepted with Pleasure                       
                        </div>
                        <div  class='programContent'>
                        <?php
                            $AcceptsSQL="Select* from tbl_rsvp WHERE response='Accepts with Pleasure'";
                            $Accepts=$con->query($AcceptsSQL);
                            if($Accepts->num_rows>0)
                            {
                                $i=1;
                                while($Accept=$Accepts->fetch_assoc())
                                {
                                  echo $i.". ".$Accept["Name"]."<br>";
                                  $i++;
                                }
                            }
                            else
                            {
                                echo"No Accepts recorded yet";
                            }
                        ?>
                        </div>
                    </div>
                    <div class='programDiv'>
                        <div class='programTitle'>
                            &nbsp;
                        </div>
                    </div>
                    <div class='programDiv'>
                        <div class='programTitle'>
                            Declined
                        </div>
                        <div  class='programContent'>
                        <?php
                            $DeclinesSQL="Select* from tbl_rsvp WHERE response='Regretfully Declines'";
                            $Declines=$con->query($DeclinesSQL);
                            if($Declines->num_rows>0)
                            {
                                $i=1;
                                while($Decline=$Declines->fetch_assoc())
                                {
                                    echo $i.". ".$Decline["Name"]."<br>";
                                    $i++;
                                }
                            }
                            else
                            {
                                echo"No Declines recorded yet";
                            }
                        ?>
                        </div>
                    </div>
                    <br><br>
                    <div class='programDiv'>
                        <div class='programTitle'>
                            Vaccination Status
                        </div>
                        <div  class='programContent'>
                        <?php
                            $VacStatSQL="Select* from tbl_vaccinestatus";
                            $VacStats=$con->query($VacStatSQL);
                            if($VacStats->num_rows>0)
                            {
                                $i=1;
                                while($VacStat=$VacStats->fetch_assoc())
                                {
                                    echo $i.". ".$VacStat["Name"]."-".$VacStat["VaccineStatus"]."<br>";

                                    $i++;
                                }
                            }
                            else
                            {
                                echo"No  recorded yet";
                            }
                        ?>
                        </div>
                    </div>
                </div>
           
                
                
                
            </div>
            <div class='footer'>
            </div>
        </div>
    </body>
</html>
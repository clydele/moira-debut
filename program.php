<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Moira's 18th Bday</title>
        <link rel="stylesheet" href="mystyle.css">
        <style>
            body{background-color:white;margin:0;}
            .programDiv{background-color:white;width:100vw;height:fit-content;}
            .programTitle{font:50pt Elegante;width:70%;color:#BF8F00;margin:auto auto;line-height:140%;}
            .programContent{font:17pt Book Antiqua;width:70%;margin:auto auto;line-height:140%;color:#680068;background-image:url('pics/programBackground.png');background-size:contain;background-position:center;background-repeat:no-repeat;}
            #desktop{display:block;}
            #mobile{display:none;}
            @media only screen and (max-width: 768px) {
                /* For mobile phones: */
                
                [class*="col-"] {
                    width: 100%;
                }
                #desktop{display:none;}
                 #mobile{display:block;}
                 .programTitle{font:45pt Elegante;}
                 .programContent{font:16pt Book Antiqua;}
            }
                </style>
    </head>
    
    <body>
        <div class='container'>
            <div class='header'>
                <div class='navigation'>
                    <div class='nav-item'>
                        <a href="index.php">HOME</a>
                        
                    </div>
                    <div class='nav-item'>
                        <a href='photos.php'>PHOTOS</a>
                    </div>
                    <div class='nav-item'style='height:90px;width:90px'>
                        <img src='pics/flower.png'>
                    </div>
                    <div class='nav-item'>
                        <a href='location.php'>LOCATION</a>
                    </div>
                    <div class='nav-item'>
                        <a href='rsvp.php'>RSVP</a>
                    </div>
                </div>
            </div>  
            <div class='body'>
                
            <div class='col-12'>
                &nbsp;
            </div> 
            <div class='col-12'>
                 &nbsp;
            </div>  

            <div class='pagetitle'>
                    <div style='font:20pt Book Antiqua;color:#680068;letter-spacing:1.5em;text-align:center'>
                    <p><b>PROGRAM</b></p>
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>

                <div class='bodycontent2'id='desktop'>
                        <img src='pics/7.PNG'>
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='bodycontent2'id='mobile'>
                    
                
                    <div class='col-12'>
                        <img src='pics/programPic.PNG'>
                    </div>
                </div>
                <div class='col-12'>
                    &nbsp;
                </div>
                <div class='bodycontent2'>
                    <div class='programDiv'>
                        <div class='programTitle'>
                        <img src='pics/PHOTOS/roses.PNG'>
                        &nbsp;
                        </div>
                        <br>
                        <div  class='programContent'>                           
                            Vincent Emil Agno<br>
                            Joackim Pether Agno<br>
                            Joshua Cedric Rafer<br>
                            Jed Rafer<br>
                            Justine Rafer<br>
                            Jaime Carlos Rafer<br>
                            Neil Patrick Agno<br>
                            Neil Marvin Agno<br>
                            Edward Joseph Garcia<br>
                            Alyster Allan Marasigan<br>
                            Ralph Manuel Rosilla<br>
                            Jeremiah Lacsamana<br>
                            Rolly Baguinon<br>
                            Harvey Paronia<br>
                            James De Leon<br>
                            Nikko Doniel Calapati<br>
                            James Rafer<br>
                            Edgardo Agno<br>
                            Noriel Agno<br>
                        </div>
                    </div>
                    <div class='programDiv'>
                        <div class='programTitle'>
                        </div>
                        &nbsp;
                    </div>
                    <br>
                    <div class='programDiv'>
                        <div class='programTitle'>
                            <img src='pics/PHOTOS/gifts.PNG'>
                        </div>
                        <br>
                        <div  class='programContent'>
                        Ms. Genalyn Barba<br>
                        Mrs. Ana Marie Marasigan<br>
                        Mr. Richard Agno<br>
                        Mr. Henry Dimayuga<br>
                        Coun. Melody Luna<br>
                        Mrs. Helen Oracion<br>
                        Mrs. Lynn Malabanan<br>
                        Ms. Rozellyn Yulatic<br>
                        Mr. Joe Borral<br>
                        Ms. Jessielyn Sentiles<br>
                        Mrs. Daisy Mae Rafer<br>
                        Mr. Neil Agno<br>
                        Mrs. Jean Rafer Deriada<br>
                        Dra. Mary Grace Jarapa<br>
                        Ms. Jane Garcia<br>
                        Mrs. Alyssa Albis<br>
                        Gen. Gerardo Gonzales<br> 
                        Mrs. Juliet Albaño<br>
                        
                        <br>
                        </div>

                    </div>

                    <div class='programDiv'>
                        <div class='programTitle'>
                            &nbsp;
                            <img src='pics/PHOTOS/candles.PNG'>
                        </div>
                        <br>
                        <div class='programContent'>
                        Serina Lexine Malabanan<br>
                        Ayanna De Lumban<br>
                        Danielle Paletinos<br>
                        Alliyah Barba<br>
                        Krystal Althea Agno<br>
                        Zhayen Kirsten Agno<br>
                        Johana Clarice Rafer<br>
                        Rina Carmela Marasigan<br>
                        Mary Grace Opeña<br>
                        Robbie Ann Arcangel<br>
                        Gaddie Mackenzie Gonzales<br>
                        Heunice Oracion<br>
                        Elonah Jean Leodones<br>
                        Regine Cyril Marasigan<br>
                        Rica Cziarra Marasigan<br>
                        Shannen Gan<br>
                        Justine Maia De Chavez<br>
                        Chezka Quiambao<br> 
                        </div>
                    &nbsp;
                    </div>

                    <div class='programDiv'>
                        <div class='programTitle'>
                            &nbsp;
                            <img src='pics/PHOTOS/wisdom.PNG'>
                        </div>
                    </div>
                    <br>
                    <div class='programContent'>
                    Miss Trish<br>
                    Miss Cosare<br>
                    Miss Aera<br>
                    Councilor Melody<br>
                    Miss Abi<br>
                    Gen Gonzales<br>
                    Jane<br>
                    Ellen<br>
                    </div>

                </div>
              
                
                
                
            </div>
            <div class='footer'>
            </div>
        </div>
    </body>
</html>